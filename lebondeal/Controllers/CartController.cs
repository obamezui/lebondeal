﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using lebondeal.Models;
using lebondeal.Services;
using lebondeal.ViewModels;

namespace lebondeal.Controllers
{
    public class CartController : Controller
    {
        // GET: Cart
        public async Task<ActionResult> Index()
        {
            var cart = new Cart(HttpContext);
            var items = await cart.GetCartItemsAsync();
            var cartViewModel = new CartViewModel
            {
                Items = items,
                Total = CalcuateCart(items)

            };
            return View(cartViewModel);
        }
        //Ajouter au panier
        public async  Task<ActionResult> AddToCart(int id)
        {
            
            var cart = new Cart(HttpContext);
            await cart.AddAsync(id);
            //retour à l'index
            return RedirectToAction("index");
        }
        //checkout 
        [HttpGet]
        public  ActionResult Checkout()
        {


            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Checkout(CheckoutViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var cart = new Cart(HttpContext);

            var result = await cart.CheckoutAsync(model);

            if (result.Succeeded)
            {
                //en cas de succes de la transaction
                TempData["transactionId"] = result.TransactionId;
                return RedirectToAction("Complete");
            }

            ModelState.AddModelError(string.Empty, result.Message);

            return View(model);
        }
        //supprimer du panier 
        public async Task<ActionResult> RemoveToCart(int id)
        {

            var cart = new Cart(HttpContext);
            await cart.RemoveAsync(id);
            //retour à l'index
            return RedirectToAction("index");
        }

        //supprimer du definitivement 
        public async Task<ActionResult> RemoveFromCart(int id)
        {

            var cart = new Cart(HttpContext);
            await cart.RemoveFromCartAsync(id);
            //retour à l'index
            return RedirectToAction("index");
        }

        //action complete lorsque le paiment est fait
        public ActionResult Complete()
        {
            ViewBag.TransactionId = (string)TempData["transactionId"];

            return View();
        }
        //fonction qui calcule le total 
        private static decimal CalcuateCart(IEnumerable<CartItem> items)
        {
            //nombre decimal
            return items.Sum(item => (item.Product.Price*item.Count));
        }
    }
}