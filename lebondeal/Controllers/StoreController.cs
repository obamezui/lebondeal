﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using lebondeal.Data;
using lebondeal.Models;
using lebondeal.Services;

namespace lebondeal.Controllers
{
    public class StoreController : Controller
    {
        //les controleurs par défaut
        private readonly StoreService _store;
        public StoreController() : this(new StoreService()) { }
        public StoreController(StoreService service)
        {
            _store = service;
            
        }
        // GET: Store
        public async Task<ActionResult> Index()
        {
            var slides = await _store.GetAllSliderAsync();
            return View(slides);
        }
        //afficher une category 
        public async Task<ActionResult> Browse(string id)
        {
            var products = await _store.GetProductsForAsync(id);

            if (!products.Any())
            {
                return HttpNotFound();
            }

            return View(products);
        }

        //page de contact
        [HttpGet]
        public  ActionResult Contact()
        {
            return View(new Message());
        }

        [HttpPost]
        public async Task <ActionResult> Contact(Message model)
        {
            if (!ModelState.IsValid)
            {
                //on retourne la vue en lui passant le model
                return View(model);

            }
                model.messageDate = DateTime.Now;
               await _store.CreateMessage(model);
                return RedirectToAction("Contact");
           
            return View();
        }

        //details d'un produit
        public async Task<ActionResult> Details(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            var product = await _store.GetProductByIdAsync(id.Value);

            if (product == null)
            {
                return HttpNotFound();
            }

            return View(product);
        }


        //electro-menager
        public async Task<ActionResult> Electro()
        {
            var products = await  _store.GetAllProductByIdAsync(3);

            return View(products);
        }

        //telephonie
        public async Task<ActionResult> Telephonie()
        {
            var products = await _store.GetAllProductByIdAsync(1);

            return View(products);
        }


        //informatique
        public async Task<ActionResult> Informatique()
        {
            var products = await _store.GetAllProductByIdAsync(8);

            return View(products);
        }

        //games
        public async Task<ActionResult> Games()
        {
            var products = await _store.GetAllProductByIdAsync(9);

            return View(products);
        }

        //hifi
        public async Task<ActionResult> Hifi()
        {
            var products = await _store.GetAllProductByIdAsync(10);

            return View(products);
        }

        //images
        public async Task<ActionResult> Picture()
        {
            var products = await _store.GetAllProductByIdAsync(11);

            return View(products);
        }


    }
}