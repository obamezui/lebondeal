﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using lebondeal.Models;

namespace lebondeal.ViewModels
{
    public class CartViewModel
    {
        public  IEnumerable<CartItem> Items { get; set; }
        public decimal Total { get; set; }
    }
}