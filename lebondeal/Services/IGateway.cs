﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using lebondeal.ViewModels;

namespace lebondeal.Services
{
    public interface IGateway
    {
        PaymentResult ProcessPayment(CheckoutViewModel model);
    }
}