﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using lebondeal.Data;
using lebondeal.Models;
using lebondeal.ViewModels;

namespace lebondeal.Services
{
    public class Cart
    {
        private readonly CockpitContext _db;
        private readonly string _cartId;

        public Cart(HttpContextBase context)
            : this(context, new CockpitContext())
        {
            
        }
        public Cart(HttpContextBase httpContext,CockpitContext context)
        {
            _db = context;
            _cartId = GetCartId(httpContext);
        }
        //ajouter un produit au panier
        public async Task AddAsync(int id)
        {
            var product = _db.Products.SingleOrDefault(p => p.ProductId == id);
            var cartItem = await _db.CartItems.SingleOrDefaultAsync(c => c.ProductId == id &&  c.CartId == _cartId);
            //on verifie si le produit est present dans le panier
            if (cartItem != null)
            {
                cartItem.Count++;
            }
            else
            {
                cartItem = new CartItem
                {
                    ProductId = id,
                    CartId = _cartId,
                    Count = 1,
                    DateCreated = DateTime.Now
                };
                _db.CartItems.Add(cartItem);
            }
            await _db.SaveChangesAsync();
        }

        //fonction pour recuperer tous les produits du panier
        public async Task<IEnumerable<CartItem>> GetCartItemsAsync()
        {
            return await _db.CartItems.Include("product").Where(c => c.CartId == _cartId).ToArrayAsync();
        }

        //supprimer un produit du panier
        public async Task<int> RemoveAsync(int id)
        {
            var itemCount = 0;
            var cartItem = await _db.CartItems.SingleOrDefaultAsync(c => c.ProductId == id && c.CartId == _cartId);
            if (cartItem == null)
            {
                return itemCount;
            }
            if (cartItem.Count>1)
            {
                cartItem.Count--;
                itemCount = cartItem.Count;
            }
            else
            {
                _db.CartItems.Remove(cartItem);
            }
            //on sauvegarde les changements 
            await _db.SaveChangesAsync();
            return itemCount;
        }
        //supprimer définitivement du panier 
        public async Task RemoveFromCartAsync(int id)
        {
            var cartItem = await _db.CartItems.SingleOrDefaultAsync(c => c.ProductId == id && c.CartId == _cartId);
          
            _db.CartItems.Remove(cartItem);
            //on sauvegarde les changements 
            await _db.SaveChangesAsync();
          
        }
        //checkout
        public async Task<PaymentResult> CheckoutAsync(CheckoutViewModel model)
        {
            var items = await GetCartItemsAsync();
            var order = new Order()
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Address = model.Address,
                City = model.City,
                State = model.State,
                PostalCode = model.PostalCode,
                Country = model.Country,
                Phone = model.Phone,
                Email = model.Email,
                OrderDate = DateTime.Now
            };

            foreach (var item in items)
            {
                var detail = new OrderDetail()
                {
                    ProductId = item.ProductId,
                    UnitPrice = item.Product.Price,
                    Quantity = item.Count
                };

                order.Total += (item.Product.Price * item.Count);

                order.OrderDetails.Add(detail);
            }

            model.Total = order.Total;

            var gateway = new PaymentGateway();
            var result = gateway.ProcessPayment(model);

            if (result.Succeeded)
            {
                order.TransactionId = result.TransactionId;
                _db.Orders.Add(order);
                //on vide le panier
                _db.CartItems.RemoveRange(items);
                await _db.SaveChangesAsync();
            }

            return result;
        }

        //fonction qui permet de gerer le cookie du panier
        private string GetCartId(HttpContextBase http)
        {
            var cookie = http.Request.Cookies.Get("shoppingcart");
            var cartId = string.Empty;
            //si il on a aucun cookie
            if (cookie == null || string.IsNullOrWhiteSpace(cookie.Value))
            {
                cookie = new HttpCookie("shoppingcart");
                cartId = Guid.NewGuid().ToString();
                cookie.Value = cartId;
                cookie.Expires = DateTime.Now.AddDays(7);
                http.Response.Cookies.Add(cookie);
            }
            else
            {
                cartId = cookie.Value;
            }
            return cartId;
        }
    }
}