﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using lebondeal.Data;
using lebondeal.Models;

namespace lebondeal.Services
{
    public class StoreService
    {
        private readonly CockpitContext _db;

        public StoreService() : this(new CockpitContext()) { }
        public StoreService(CockpitContext context)
        {
            _db = context;
        }

        public async Task<IEnumerable<Category>> GetCategoriesAsync()
        {
            return await _db.Categories.OrderBy(c => c.Name).ToArrayAsync();
        }

        public async Task<IEnumerable<Product>> GetProductsForAsync(string category)
        {
            return await _db.Products.Include("Category")
                .Where(p => p.Category.Name == category).ToArrayAsync();
        }

       

        public async Task<Product> GetProductByIdAsync(int id)
        {
            return await _db.Products.Include("Category")
                .Where(p => p.ProductId == id).SingleOrDefaultAsync();
        }

        public async Task<IEnumerable<Product>> GetAllProductByIdAsync(int id)
        {
            return await _db.Products
                
                .Where(p => p.CategoryId == id).ToArrayAsync();
        }

        public async Task CreateMessage(Message ms)
        {
            _db.Messages.Add(ms);
            _db.SaveChanges();
        }

        public async Task DeleteMessage(Message ms)
        {
            _db.Messages.Remove(ms);
            _db.SaveChanges();
        }

        //fonction qui permet de recupérer tous les slides actifs
        public async Task<IEnumerable<Slider>> GetAllSliderAsync()
        {
            return await _db.Sliders
                .Where(s => s.isActivate==true).ToArrayAsync();
        }

      

    }
}