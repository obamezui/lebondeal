﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Braintree;
using lebondeal.Services;
using lebondeal.ViewModels;

namespace lebondeal.Services
{
    public class PaymentGateway:IGateway
    {
        private readonly BraintreeGateway _gateway = new BraintreeGateway
        {
            Environment = Braintree.Environment.PRODUCTION,
            MerchantId = "mw2q3pskhb5g6v5k",
            PublicKey = "rb8p8vhthv6vvb2b",
            PrivateKey = "1826c466ba2be7007f431bde6460fd98"
        };
        public PaymentResult ProcessPayment(CheckoutViewModel model)
        {
            var request = new TransactionRequest()
            {
                Amount = model.Total,
                CreditCard = new TransactionCreditCardRequest()
                {
                    Number = model.CardNumber,
                    CVV = model.Cvv,
                    ExpirationMonth = model.Month,
                    ExpirationYear = model.Year
                },
                Options = new TransactionOptionsRequest()
                {
                    SubmitForSettlement = true
                }
            };

            var result = _gateway.Transaction.Sale(request);

            if (result.IsSuccess())
            {
                return new PaymentResult(result.Target.Id, true, null);
            }

            return new PaymentResult(null, false, result.Message);
        }
    }
}