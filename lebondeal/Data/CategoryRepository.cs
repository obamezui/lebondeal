﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using lebondeal.Models;

namespace lebondeal.Data
{
    public class CategoryRepository : ICategoryRepository
    {
        public IEnumerable<Category> GetAll()
        {
            using (var db = new CockpitContext())
            {
                return db.Categories
                    .OrderByDescending(c => c.Name)
                    .ToArray();
            }
        }

        public Category Get(int id)
        {
            using (var db = new CockpitContext())
            {
                return db.Categories.SingleOrDefault(c => c.CategoryId == id);
            }
        }

        public void Edit(Category updatedItem, int id)
        {
            //on implemente le contexte
            using (var db = new CockpitContext())
            {
                //on recupere le produit en fonction de son id
                var category = db.Categories.SingleOrDefault(c => c.CategoryId == id);
                if (category == null)
                {
                    throw new KeyNotFoundException("la categorie n° "
                        + id + " n/'est pas enregistrée dans notre base de données");
                }

                category.CategoryId = updatedItem.CategoryId;
                category.Name = updatedItem.Name;
                category.CategoryImage = updatedItem.CategoryImage;
                db.SaveChanges();

            }
        }

        public void Create(Category model)
        {
            using (var db = new CockpitContext())
            {
                var category = db.Categories.SingleOrDefault(c => c.CategoryId == model.CategoryId);
                if (category != null)
                {
                    throw new ArgumentException("cette categorie existe déja");
                }
                db.Categories.Add(model);
                db.SaveChanges();
            }
        }


        //supprimer une catégorie
        public void Remove(int id)
        {
            using (var db = new CockpitContext())
            {
                var category = db.Categories.SingleOrDefault(c => c.CategoryId == id);

                if (category == null)
                {
                    throw new KeyNotFoundException("la categorie  " + id + " n'existe pas ");
                }

                db.Categories.Remove(category);
                db.SaveChanges();
            }
        }
    }
}