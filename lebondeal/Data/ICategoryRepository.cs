﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lebondeal.Models;

namespace lebondeal.Data
{
    public interface ICategoryRepository
    {

        IEnumerable<Category> GetAll();
        Category Get( int id);
        //fonction qui permet d'éditer une categorie
        void Edit(Category updatedItem, int id);
        //fonction qui permet de creer une categorie
        void Create(Category model);
        void Remove(int id);
    }
}
