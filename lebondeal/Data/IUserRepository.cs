﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using lebondeal.Models;

namespace lebondeal.Data
{
   public  interface IUserRepository : IDisposable
    {
        Task<User> GetUserByNameAsync(string username);
        Task<IEnumerable<User>> GetAllUsersAsync();
        Task CreateAsync(User user, string password);
        Task DeleteAsync(User user);
        Task UpdateAsync(User user);
        bool VerifyUserPassword(string hashedPassword, string providedPassword);
        string HashPassword(string password);
        Task AddUserToRoleAsync(User newUser, string role);
        Task<IEnumerable<string>> GetRolesForUserAsync(User user);

       Task RemoveRoleForUserAsync(User user, params string[] rolenames);


       Task<User> GetLoginUserAsync(string userName, string password);
        Task<ClaimsIdentity> CreateAsync(User user);
    }
}
