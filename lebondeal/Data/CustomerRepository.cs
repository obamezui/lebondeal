﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using lebondeal.Models;

namespace lebondeal.Data
{
    public class CustomerRepository : ICustomerRepository
    {
        public IEnumerable<Customer> GetAllCustomers()
        {
            using (var db = new CockpitContext())
            {
               return db.Customers.
                    OrderByDescending(c => c.customer_lastname)
                    .ToArray();
            }
        }

        public Customer GetCustomerById(int id)
        {
            using (var db = new CockpitContext())
            {
                return db.Customers
                    .SingleOrDefault(c => c.customer_id == id);

            }
        }

        public void Create(Customer model)
        {
            using (var db = new CockpitContext())
            {
                var customer = db.Customers.SingleOrDefault(c => c.customer_id == model.customer_id);
                if (customer != null)
                {
                    throw new ArgumentException("ce client existe deja ");
                }
                model.customer_active = true;
                model.customer_createdate = DateTime.Now;
                model.customer_lastupdate = DateTime.Now;
                db.Customers.Add(model);
                db.SaveChanges();
            }
        }

        public void Edit(Customer updatedCustomer, int id)
        {
            //on implemente le contexte
            using (var db = new CockpitContext())
            {
                //on recupere le client en fonction de son id
                var customer = db.Customers.SingleOrDefault(c => c.customer_id == id);
                if (customer == null)
                {
                    throw new KeyNotFoundException("le client  n° "
                        + id + " n/'est pas enregistrée dans notre base de données");
                }

                customer.customer_id = updatedCustomer.customer_id;
                customer.customer_firstname = updatedCustomer.customer_firstname;
                customer.customer_lastname = updatedCustomer.customer_lastname;
                customer.address = updatedCustomer.address;
                customer.address2 = updatedCustomer.address2;
                customer.phone = updatedCustomer.phone;
                customer.city_name = updatedCustomer.city_name;
                customer.country_name = updatedCustomer.country_name;
                customer.postal_code = updatedCustomer.postal_code;
                customer.customer_active = updatedCustomer.customer_active;
                customer.customer_email = updatedCustomer.customer_email;
                customer.customer_lastupdate = DateTime.Now;
                db.SaveChanges();

            }
        }

        public bool Exist(int id)
        {
            
            using (var db = new CockpitContext())
            {
                bool exist = false;

                var customer = db.Customers.SingleOrDefault(c => c.customer_id == id);

                if (customer != null)
                {
                    exist = true;
                }

                return exist;
            }

        }

        public void RemoveCustomer(int id)
        {
            using (var db = new CockpitContext())
            {
                var customer = db.Customers.SingleOrDefault(c => c.customer_id == id);

                if (customer == null)
                {
                    throw new KeyNotFoundException("le n°" + id + " n'existe pas ");
                }

                db.Customers.Remove(customer);
                db.SaveChanges();
            }
        }
    }
}