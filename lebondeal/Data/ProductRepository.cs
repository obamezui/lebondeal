﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using lebondeal.Models;

namespace lebondeal.Data
{
    public class ProductRepository : IProductRepository
    {
        public Product Get(int id)
        {
            using (var db = new CockpitContext())
            {
                return db.Products.Include("Category").SingleOrDefault(product => product.ProductId == id);
            }
        }

        public void Edit(Product updatedItem, int id)
        {
            //on implemente le contexte
            using (var db = new CockpitContext())
            {
                //on recupere le produit en fonction de son id
                var product = db.Products.SingleOrDefault(p => p.ProductId == id);
                if (product==null)
                {
                   throw new KeyNotFoundException("le produit n° "
                       +id+" n/'est pas enregistré dans notre base de données");
                }

                product.ProductId = updatedItem.ProductId;
                product.Category = updatedItem.Category;
                product.Name = updatedItem.Name;
                product.CategoryId = updatedItem.CategoryId;
                product.Description = updatedItem.Description;
                product.ImgUrl = updatedItem.ImgUrl;
                product.Price = updatedItem.Price;
                db.SaveChanges();

            }
        }
        //créer un produit
        public void Create(Product model)
        {
            using (var db = new CockpitContext())
            {
                var product = db.Products.SingleOrDefault(p => p.ProductId == model.ProductId);
                if (product != null)
                {
                    throw  new ArgumentException("ce produit existe déja");
                }
                db.Products.Add(model);
                db.SaveChanges();
            }
        }
        //recupérer tous les produits
        public IEnumerable<Product> GetAll()
        {
            using (var db = new CockpitContext())
            {
                return db.Products.Include("Category")
                    .OrderByDescending(product => product.Name)
                    .ToArray();
            }
        }
        //vérifier l'existence d'un produit
        public bool Exists(int product)
        {
            throw new NotImplementedException();
        }
        //supprimer un produit
        public void RemoveProduct(int id)
        {
            using (var db = new CockpitContext())
            {
                var product = db.Products.SingleOrDefault(p => p.ProductId == id);

                if (product == null)
                {
                    throw new KeyNotFoundException("ce produit  " + id + " n'existe pas ");
                }

                db.Products.Remove(product);
                db.SaveChanges();
            }
        }
    }
}