﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using lebondeal.Models;

namespace lebondeal.Data
{
    public class MessageRepository : IMessageRepository
    {
        public void Create(Message model)
        {
            using (var db = new CockpitContext())
            {
               
                db.Messages.Add(model);
                db.SaveChanges();
            }
        }
    }
}