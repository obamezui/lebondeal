﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lebondeal.Models;

namespace lebondeal.Data
{
   public interface ISliderRepository
    {
        IEnumerable<Slider> GetAll();
        //fonction pour recuperer un slide selon son id
        Slider Get(int id);
        //fonction qui permet d'éditer un slide
        void Edit(Slider updatedItem, int id);
        //fonction qui permet de creer un slide
        void Create(Slider model);
        void Remove(int id);
    }
}
