﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lebondeal.Models;

namespace lebondeal.Data
{
   public  interface ICustomerRepository
   {
       IEnumerable<Customer> GetAllCustomers();
       Customer GetCustomerById(int id);
       void Create(Customer model);
       void Edit(Customer updatedCustomer, int id);
       bool Exist(int id);
       void RemoveCustomer(int id);
   }
}
