﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using lebondeal.Models;

namespace lebondeal.Data
{
    public class SliderRepository:ISliderRepository
    {
        public IEnumerable<Slider> GetAll()
        {
            using (var db = new CockpitContext())
            {
                return db.Sliders
                    .OrderByDescending(s => s.Title)
                    .ToArray();
            }
        }

        public Slider Get(int id)
        {
            using (var db = new CockpitContext())
            {
                return db.Sliders.SingleOrDefault(s => s.Id == id);
            }
        }

        public void Edit(Slider updatedItem, int id)
        {
            using (var db = new CockpitContext())
            {
                //on recupere le produit en fonction de son id
                var slider = db.Sliders.SingleOrDefault(s => s.Id == id);
                if (slider == null)
                {
                    throw new KeyNotFoundException("le slider n° "
                        + id + " n/'est pas enregistrée dans notre base de données");
                }

                slider.Id = updatedItem.Id;
                slider.Title = updatedItem.Title;
                slider.Text = updatedItem.Text;
                slider.Img = updatedItem.Img;
                slider.isActivate = updatedItem.isActivate;
                db.SaveChanges();

            }
        }

        public void Create(Slider model)
        {
            using (var db = new CockpitContext())
            {
                var slider = db.Sliders.SingleOrDefault(s => s.Id == model.Id);
                if (slider != null)
                {
                    throw new ArgumentException("ce slide existe déja");
                }
                db.Sliders.Add(model);
                db.SaveChanges();
            }
        }

        public void Remove(int id)
        {
            using (var db = new CockpitContext())
            {
                var slider = db.Sliders.SingleOrDefault(s => s.Id == id);

                if (slider == null)
                {
                    throw new KeyNotFoundException("le slide n°  " + id + " n'existe pas ");
                }

                db.Sliders.Remove(slider);
                db.SaveChanges();
            }
        }
    }
}