﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lebondeal.Models;

namespace lebondeal.Data
{
    public interface IProductRepository
    {
        //fonction qui permet de recuper un produit
        Product Get(int id);
        //fonction qui permet d'éditer un produit
        void Edit(Product updatedItem, int id);
        //fonction qui permet de creer un produit
        void Create(Product model);
        //fonction qui permet de recuperer tous les produit
        IEnumerable<Product> GetAll();
        //fonction qui permet de verifier l'existence d'un produit
        bool Exists(int product);
        //fonction pour supprimer un produit
        void RemoveProduct(int  id);
    }
}
