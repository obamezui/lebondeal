﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using lebondeal.Models;
using Microsoft.AspNet.Identity;

namespace lebondeal.Data
{
    public class UserRepository : IUserRepository
    {
        private readonly CockpitUserStore _store;
        private readonly CockpitUserManager _manager;
        //constructeur de la classe
        public UserRepository()
        {
            _store = new CockpitUserStore();
            _manager = new CockpitUserManager(_store);
        }

        public async Task<User> GetUserByNameAsync(string username)
        {
            return await  _store.FindByNameAsync(username);
        }

        public async Task<IEnumerable<User>> GetAllUsersAsync()
        {
            return await _store.Users.ToArrayAsync();
        }

        public async Task CreateAsync(User user, string password)
        {
            await _manager.CreateAsync(user, password);
        }

        public async Task DeleteAsync(User user)
        {
            await _manager.DeleteAsync(user);
        }

        public async Task UpdateAsync(User user)
        {
            await _manager.UpdateAsync(user);
         
        }

        public bool VerifyUserPassword(string hashedPassword, string providedPassword)
        {
            return _manager.PasswordHasher.VerifyHashedPassword(hashedPassword, providedPassword) ==
                   PasswordVerificationResult.Success;
        }

        public string HashPassword(string password)
        {
            return _manager.PasswordHasher.HashPassword(password);
        }

        public async Task AddUserToRoleAsync(User newUser, string role)
        {
            await _manager.AddToRoleAsync(newUser.Id, role);
        }

        public async Task<IEnumerable<string> > GetRolesForUserAsync(User user)
        {
          return   await _manager.GetRolesAsync(user.Id);
        }

        public async Task RemoveRoleForUserAsync(User user, params string[] rolenames)
        {
             await _manager.RemoveFromRolesAsync(user.Id, rolenames);
        }

       public async  Task<User> GetLoginUserAsync(string userName, string password)
        {
            return await _manager.FindAsync(userName, password);
        }

        public async Task<ClaimsIdentity> CreateAsync(User user)
        {
            return await _manager.CreateIdentityAsync(user,DefaultAuthenticationTypes.ApplicationCookie);
        }



        private bool _disposed = false;
        public void Dispose()
        {
            if (!_disposed)
            {
                _manager.Dispose();
                _store.Dispose();
            }

            _disposed = true;
        }

    }
}