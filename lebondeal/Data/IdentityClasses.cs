﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using lebondeal.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace lebondeal.Data
{
    //classe userstore
    public class CockpitUserStore : UserStore<User>
    {
        public CockpitUserStore() : this(new CockpitContext())
        { }
        public CockpitUserStore(CockpitContext context) : base(context)
        {
            
        }
    }

    //classe user manager
    public class CockpitUserManager : UserManager<User>
    {
        public CockpitUserManager() : this(new CockpitUserStore())
        { }
        public CockpitUserManager(UserStore<User> userStore) : base(userStore)
        {

        }
    }
}