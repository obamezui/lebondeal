namespace lebondeal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatecustomers : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Cities", "country_id", "dbo.Countries");
            DropForeignKey("dbo.Addresses", "city_id", "dbo.Cities");
            DropIndex("dbo.Addresses", new[] { "city_id" });
            DropIndex("dbo.Cities", new[] { "country_id" });
            AlterColumn("dbo.Customers", "city_name", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Countries",
                c => new
                    {
                        country_id = c.Int(nullable: false, identity: true),
                        country_name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.country_id);
            
            CreateTable(
                "dbo.Cities",
                c => new
                    {
                        city_id = c.Int(nullable: false, identity: true),
                        city_name = c.Int(nullable: false),
                        country_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.city_id);
            
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        address_id = c.Int(nullable: false, identity: true),
                        address = c.String(nullable: false),
                        address2 = c.String(),
                        district = c.String(nullable: false),
                        postal_code = c.String(nullable: false),
                        phone = c.String(nullable: false),
                        last_update = c.String(),
                        city_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.address_id);
            
            AlterColumn("dbo.Customers", "city_name", c => c.Int(nullable: false));
            CreateIndex("dbo.Cities", "country_id");
            CreateIndex("dbo.Addresses", "city_id");
            AddForeignKey("dbo.Addresses", "city_id", "dbo.Cities", "city_id", cascadeDelete: true);
            AddForeignKey("dbo.Cities", "country_id", "dbo.Countries", "country_id", cascadeDelete: true);
        }
    }
}
