namespace lebondeal.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class customer : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customers", "Phone", c => c.String());
            AddColumn("dbo.Customers", "address", c => c.String());
            AddColumn("dbo.Customers", "address2", c => c.String());
            AddColumn("dbo.Customers", "postal_code", c => c.String());
            AddColumn("dbo.Customers", "country_name", c => c.String());
        }
        
        public override void Down()
        {
        }
    }
}
