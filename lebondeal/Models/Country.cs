﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace lebondeal.Models
{
    public class Country
    {
        [Key]
        [Required]
        public int country_id { get; set; }
        [Required]
        public string country_name { get; set; }

    }
}