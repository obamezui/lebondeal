﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace lebondeal.Models
{
    public class Message
    {
        [Key]
        public int messageId { get; set; }
        [Required]
        public string messageName { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public string messageEmail { get; set; }
        [Required]
        public string messageSubject { get; set; }
        [Required]
        public string messageContent { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime messageDate { get; set; }


    }
}