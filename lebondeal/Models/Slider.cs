﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace lebondeal.Models
{
    public class Slider
    {
       [Key]
        public int Id { get; set; }
        [Required]
        public string Text { get; set; }
        [Required]
        public string Title { get; set; }
        public string Img { get; set; }
        public bool isActivate { get; set; }
    }
}