﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace lebondeal.Models
{
    public class Product
    {
            [Key]
            [Required]
            public int ProductId { get; set; }
            [Required]
            public string Name { get; set; }
            [Required]
            public string Description { get; set; }
            [Required]
            public decimal Price { get; set; }
            [Required]
            public String ImgUrl { get; set; }
            public int CategoryId { get; set; }
            [ForeignKey("CategoryId")]
            public virtual  Category Category { get; set; }
        
    }
}