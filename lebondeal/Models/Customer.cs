﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace lebondeal.Models
{
    public class Customer
    {
        [Key]
        [Required]
        public int customer_id { get; set; }
        [Required]
        public string customer_firstname { get; set; }
        [Required]
        public string customer_lastname { get; set; }
        [Required]
        public string phone { get; set; }
        [Required]
        public string customer_email { get; set; }
        [Required]
        public string address { get; set; }
        public string address2 { get; set; }
        [Required]
        public string city_name { get; set; }
        [Required]
        public string postal_code { get; set; }
        [Required]
        public string country_name { get; set; }
        public Boolean customer_active { get; set; }
        public DateTime customer_createdate { get; set; }
        public DateTime customer_lastupdate { get; set; }
    }
}