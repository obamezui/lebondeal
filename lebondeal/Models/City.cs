﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace lebondeal.Models
{
    public class City
    {
        [Key]
        [Required]
        public int city_id { get; set; }
        [Required]
        public int city_name { get; set; }
        public int country_id { get; set; }
        [ForeignKey("country_id")]
        public virtual Country country { get; set; }

    }
}