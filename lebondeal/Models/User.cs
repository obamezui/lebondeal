﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.EntityFramework;

namespace lebondeal.Models
{
   // [RouteArea("Cockpit")]
   // [RoutePrefix("user")]
    public class User:IdentityUser
    {
       
        public string DisplayName { get; set; }
    }
}