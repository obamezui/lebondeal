﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace lebondeal.Models
{
    public static  class StringExtensions
    {
        //cette fonction permet de formater l'url 
        public static string MakeUrlFriendly(this string value)
        {
            value = value.ToLowerInvariant().Replace(" ", "-");
            //regex
            value = Regex.Replace(value, @"[^0-9a-z-]", string.Empty);
            return value;
        }
    }
}