﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace lebondeal.Models
{
    public class BillingAddress
    {
        [Key]
        [Required]
        public int address_id { get; set; }
        [Required]
        public string address { get; set; }
        public string address2 { get; set; }
        [Required]
        public string district { get; set; }
        [Required]
        public string postal_code { get; set; }
        [Required]
        public string phone { get; set; }
        public string last_update { get; set; }
        public int city_id { get; set; }
        [ForeignKey("city_id")]
        public virtual City City { get; set; }
    }
}