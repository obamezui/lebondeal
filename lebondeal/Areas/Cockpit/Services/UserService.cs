﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.ModelBinding;
using lebondeal.Areas.Cockpit.ViewModels;
using lebondeal.Data;
using lebondeal.Models;

namespace lebondeal.Areas.Cockpit.Services
{
    public class UserService
    {
        private readonly IUserRepository _users;
        private readonly IRoleRepository _roles;
        private readonly System.Web.Mvc.ModelStateDictionary _modelState;

        public UserService(System.Web.Mvc.ModelStateDictionary modelState, IUserRepository userRepository, IRoleRepository roleRepository)
        {
            _modelState = modelState;
            _users = userRepository;
            _roles = roleRepository;
        }

        public async Task<UserViewModel> GetUserByNameAsync(string name)
        {
            var user = await _users.GetUserByNameAsync(name);

            if (user == null)
            {
                return null;
            }

            var viewModel = new UserViewModel
            {
                UserName = user.UserName,
                Email = user.Email,
                
            };
            var roles = await _users.GetRolesForUserAsync(user);
            //on teste si on a plus d'un role pour l'utilisateur
            viewModel.selectedRole = roles.Count() > 1 ? 
                roles.FirstOrDefault() : roles.SingleOrDefault();
            viewModel.LoadUserRole(await  _roles.GetAllRolesAsync());

            return viewModel;
        }

        public async Task<bool> CreateAsync(UserViewModel model)
        {
            if (!_modelState.IsValid)
            {
                return false;
            }

            var existingUser = await _users.GetUserByNameAsync(model.UserName);

            if (existingUser != null)
            {
                _modelState.AddModelError(string.Empty, "cet utilisateur existe déja!");
                return false;
            }

            if (string.IsNullOrWhiteSpace(model.NewPassword))
            {
                _modelState.AddModelError(string.Empty, "Veuillez entrer un mot de passe .");
                return false;
            }

            var newUser = new User
            {
               
                Email = model.Email,
                UserName = model.UserName
            };

            await _users.CreateAsync(newUser, model.NewPassword);
            await _users.AddUserToRoleAsync(newUser, model.selectedRole);

            return true;
        }

        public async Task<bool> UpdateUser(UserViewModel model)
        {
            var user = await _users.GetUserByNameAsync(model.UserName);
            

            if (user == null)
            {
                _modelState.AddModelError(string.Empty, "Cet utilisateur n'existe pas.");
                return false;
            }

            if (!_modelState.IsValid)
            {
                return false;
            }

            if (!string.IsNullOrWhiteSpace(model.NewPassword))
            {
                if (string.IsNullOrWhiteSpace(model.CurrentPassword))
                {
                    _modelState.AddModelError(string.Empty, "Le mot de passe doit être renseigné");
                    return false;
                }

                var passwordVerified = _users.VerifyUserPassword(user.PasswordHash, model.CurrentPassword);

                if (!passwordVerified)
                {
                    _modelState.AddModelError(string.Empty, "Ce mot de passe n'existe pas.");
                    return false;
                }

                var newHashedPassword = _users.HashPassword(model.NewPassword);

                user.PasswordHash = newHashedPassword;
            }

            user.Email = model.Email;
           
            await _users.UpdateAsync(user);
            var roles = await _users.GetRolesForUserAsync(user);
            await _users.RemoveRoleForUserAsync(user,roles.ToArray());
            await _users.AddUserToRoleAsync(user,model.selectedRole);

            return true;

        }

        public async Task DeleteAsync(string username)
        {
            var user = await _users.GetUserByNameAsync(username);

            if (user == null)
            {
                return;
            }

            await _users.DeleteAsync(user);
        }
    }
}