﻿using System.Web.Mvc;

namespace lebondeal.Areas.Cockpit
{
    public class CockpitAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Cockpit";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Cockpit_default",
                "Cockpit/{controller}/{action}/{id}",
                new { action = "Index", controller = "bunker", id = UrlParameter.Optional }
            );
        }
    }
}