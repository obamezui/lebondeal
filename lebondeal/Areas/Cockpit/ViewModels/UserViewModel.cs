﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity.EntityFramework;

namespace lebondeal.Areas.Cockpit.ViewModels
{
    public class UserViewModel
    {
        private readonly List<string> _roles = new List<string>();
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Email { get; set; }
        public string CurrentPassword { get; set; }
        [System.ComponentModel.DataAnnotations.Compare("ConfirmPassword",ErrorMessage = "les mots de passes ne sont pas identiques")]
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
        public string selectedRole { get; set; }

        public IEnumerable<SelectListItem> Roles
        {
            get { return  new SelectList(_roles);}
        }
        //fonction qui permet de charger les roles 
        public void LoadUserRole(IEnumerable<IdentityRole> roles)
        {
            _roles.AddRange(roles.Select(r=> r.Name));
        }
    }
}