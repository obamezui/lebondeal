﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace lebondeal.Areas.Cockpit.ViewModels
{
    public class BunkerViewItem
    {
        public  string Text { get; set; }
        public string Action { get; set; }
        public object RouteInfo { get; set; }
    }
}