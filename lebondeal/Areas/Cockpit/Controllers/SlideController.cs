﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using lebondeal.Data;
using lebondeal.Models;

namespace lebondeal.Areas.Cockpit.Controllers
{
    [RouteArea("Cockpit")]
    [RoutePrefix("Slide")]
    [Authorize]
    public class SlideController : Controller
    {
        private readonly ISliderRepository _repository;
        //les constructeurs
        public SlideController():this(new SliderRepository())
        { }

        public SlideController(ISliderRepository ipr)
        {
            _repository = ipr;
        }
        // GET: Cockpit/Slide
        public ActionResult Index()
        {
            //on recupère tous les slides 
            var slides = _repository.GetAll();
            return View(slides);
        }

        //Creation d'un slide
        [HttpGet]
        [Route("create")]
        public ActionResult Create()
        {


            return View(new Slider());
        }

        [HttpPost]
        [Route("create")]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Slider model, HttpPostedFileBase file)
        {
           
            //on test l'etat du model 
            if (!ModelState.IsValid)
            {
                //on retourne la vue en lui passant le model
                return View(model);

            }

            try
            {
                if (file != null)
                {
                    string pic = System.IO.Path.GetFileName(file.FileName);

                    string path = System.IO.Path.Combine(
                                           Server.MapPath("~/Areas/Cockpit/Images/Slides"), pic);
                    // file is uploaded
                    file.SaveAs(path);
                    //on affecte le l'extension de l'image dans le model
                    model.Img = file.FileName;
                  

                }
               
                _repository.Create(model);
                return RedirectToAction("index");
            }
            catch (Exception e)
            {

                ModelState.AddModelError("key", e);
                return View(model);
            }

        }

        //edition d'une catégorie
        [HttpGet]
        [Route("edit/{id}")]
        public ActionResult Edit(int slideID)
        {
            //on recupère le slide correspondant  à l'id
            var slide = _repository.Get(slideID);

            //si le produit existe pas dans la base 
            if (slide == null)
            {
                return HttpNotFound();
            }

            return View(slide);
        }

        [HttpPost]
        [Route("edit/{id}")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Slider model, int Id, HttpPostedFileBase file)
        {
            //on recupère le produit correspondant à l'id
            var slide = _repository.Get(Id);
            //on verifie si le produit existe 
            if (slide == null)
            {
                return HttpNotFound();
            }
            //on test l'etat du model 
            if (!ModelState.IsValid)
            {
                //on retourne la vue en lui passant le model
                return View(model);

            }

            //on fait la mise à jour du produit


            try
            {
                if (file==null)
                {

                    model.Img = slide.Img;

                }
                else
                {
                   string fullPath = Request.MapPath("~/Areas/Cockpit/Images/Slides" + slide.Img);
                    if (System.IO.File.Exists(fullPath))
                    {
                        System.IO.File.Delete(fullPath);
                    }
                    string pic = System.IO.Path.GetFileName(file.FileName);

                    string path = System.IO.Path.Combine(
                        Server.MapPath("~/Areas/Cockpit/Images/Slides"), pic);
                    // file is uploaded
                    file.SaveAs(path);
                    model.Img = file.FileName;
                }
               
               
                _repository.Edit(model, Id);
                return RedirectToAction("index");
            }
            catch (KeyNotFoundException e)
            {


                return HttpNotFound();
            }
            catch (Exception e)
            {

                ModelState.AddModelError("key", e);
                return View(model);
            }

        }


        //suppression d'une categorie
        [Route("Remove/{slideID}")]
        public ActionResult Remove(int slideID)
        {

            var slide = _repository.Get(slideID);
            _repository.Remove(slideID);
            string fullPath = Request.MapPath("~/Areas/Cockpit/Images/Slides" + slide.Img);
            if (System.IO.File.Exists(fullPath))
            {
                System.IO.File.Delete(fullPath);
            }

            return RedirectToAction("index");
        }
    }
}