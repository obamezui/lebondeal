﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using lebondeal.Areas.Cockpit.Services;
using lebondeal.Areas.Cockpit.ViewModels;
using lebondeal.Data;
using lebondeal.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace lebondeal.Areas.Cockpit.Controllers
{
    [RouteArea("Cockpit")]
    [RoutePrefix("user")]
    [Authorize]
    public class UserController : Controller
    {
        private readonly IUserRepository _userRepository;
        private readonly IRoleRepository _roleRepository;
        private readonly UserService _users;
        public UserController()
        {
            _userRepository = new UserRepository();
            _roleRepository = new RoleRepository();
            _users = new UserService(ModelState, _userRepository, _roleRepository);
        }
        // GET: Cockpit/User
        [Route("")]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> Index()
        {
            var users = await _userRepository.GetAllUsersAsync();
         
            return View(users);
        }

        //creation d'un utilisateur
        [Route("create")]
        [HttpGet]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> Create()
        {
            var model = new UserViewModel();
            model.LoadUserRole(await _roleRepository.GetAllRolesAsync());

            return View(model);
        }

        [Route("create")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> Create(UserViewModel model)
        {
         
           
            var completed = await _users.CreateAsync(model);

            if (completed)
            {
                return RedirectToAction("index");
            }

            return View(model);
        }

        //edition des utilisateurs 
        [Route("edit/{username}")]
        [HttpGet]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> Edit(string username)
        {
            var user = await _users.GetUserByNameAsync(username);
            //on recupère la personne  connecté est un admin
            var currentUser = User.Identity.Name;
            if (!User.IsInRole("admin") && !string.Equals(currentUser,username,StringComparison.CurrentCultureIgnoreCase))
            {
                return new HttpUnauthorizedResult();
            }

            if (user == null)
            {
                return HttpNotFound();
            }

            return View(user);
        }

        [Route("edit/{username}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> Edit(UserViewModel model)
        {
            var userUpdated = await _users.UpdateUser(model);

            if (userUpdated)
            {
                return RedirectToAction("index");
            }

            return View(model);
        }
        //suppression d'un utilisateur
        [Route("delete/{username}")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "admin")]
        public async Task<ActionResult> Delete(string username)
        {
            await _users.DeleteAsync(username);

            return RedirectToAction("index");
        }

        private bool _isDisposed;
        protected override void Dispose(bool disposing)
        {

            if (!_isDisposed)
            {
                _userRepository.Dispose();
                _roleRepository.Dispose();
            }
            _isDisposed = true;

            base.Dispose(disposing);
        }
    }
}