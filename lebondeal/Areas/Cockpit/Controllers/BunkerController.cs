﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using lebondeal.Areas.Cockpit.ViewModels;
using lebondeal.Data;
using Microsoft.Owin.Security;

namespace lebondeal.Areas.Cockpit.Controllers
{
    [RouteArea("Cockpit")]
    [Authorize]
    public class BunkerController : Controller
    {
        //déclaration des attributs de la classe 
        private readonly IUserRepository _users;
        //les constructeurs
        public BunkerController():this(new UserRepository())
        { }

        public BunkerController(IUserRepository iuserRepository )
        {
            _users = iuserRepository;
        }
        // GET: Cockpit/Bunker
        public ActionResult Index()
        {

            return View();
        }
        //action login 
        [HttpGet]
        [Route("login")]
        [AllowAnonymous]
        public async Task<ActionResult> Login()
        {
            return View();
        }
        //on fait appelle à la classe owin pour nous connecter
        [HttpPost]
        [Route("login")]
        [AllowAnonymous]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            var user = await _users.GetLoginUserAsync(model.UserName,model.Password);
            if (user == null)
            {
                ModelState.AddModelError(string.Empty,"utilisateur inexistant dans la base de données");
            }

            
            
            //on recupere le contexte owin
            var authManager = HttpContext.GetOwinContext().Authentication;
            var userIdentity = await _users.CreateAsync(user);
            //on enregistre l'utilisateur
            // ispersistent permet de persister la connection
            authManager.SignIn(new AuthenticationProperties(){IsPersistent = model.rememberMe},userIdentity);
            return RedirectToAction("Index");
        }

        [Route("logout")]
        [AllowAnonymous]
        public async Task<ActionResult> Logout()
        {
            var authManager = HttpContext.GetOwinContext().Authentication;
            authManager.SignOut();
            return RedirectToAction("index");
        }
        [AllowAnonymous]
        public async Task<PartialViewResult> BunkerMenu()
        {
            var items = new List<BunkerViewItem>();
            //si l'utilisateur est authentifié
            if (User.Identity.IsAuthenticated)
            {
                
                items.Add(new BunkerViewItem
                {
                    Text ="Bunker",
                    Action ="index",
                    RouteInfo =  new {controller="bunker",area="cockpit"}
                    
                });
                items.Add(new BunkerViewItem
                {
                    Text = "Ventes",
                    Action = "index",
                    RouteInfo = new { controller = "store", area = "cockpit" }

                });
                items.Add(new BunkerViewItem
                {
                    Text = "Produits",
                    Action = "index",
                    RouteInfo = new { controller = "product", area = "cockpit" }

                });
                items.Add(new BunkerViewItem
                {
                    Text = "Categories",
                    Action = "index",
                    RouteInfo = new { controller = "Category", area = "cockpit" }

                });
                items.Add(new BunkerViewItem
                {
                    Text = "Clients",
                    Action = "index",
                    RouteInfo = new { controller = "Customer", area = "cockpit" }

                });
            }
            //si l'utilisateur est un admin
            if (User.IsInRole("admin"))
            {
               
                items.Add(new BunkerViewItem
                {
                    Text = "user",
                    Action = "index",
                    RouteInfo = new {controller = "user", area = "cockpit"}

                });

            }
            else
            {
                items.Add(new BunkerViewItem
                {
                    Text = "Profile",
                    Action = "edit",
                    RouteInfo = new { controller = "user", area = "cockpit" ,username=User.Identity.Name}

                });
            }
            return PartialView(items);
        }
        private bool _isDisposed;
        protected override void Dispose(bool disposing)
        {

            if (!_isDisposed)
            {
                _users.Dispose();
                
            }
            _isDisposed = true;

            base.Dispose(disposing);
        }
    }
}