﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using lebondeal.Data;
using lebondeal.Models;

namespace lebondeal.Areas.Cockpit.Controllers
{
    [RouteArea("Cockpit")]
    [RoutePrefix("Customer")]
    [Authorize]
    public class CustomerController : Controller
    {
        private readonly ICustomerRepository _repository;
        //les constructeurs
        public CustomerController():this(new CustomerRepository())
        { }

        public CustomerController(ICustomerRepository icr)
        {
            _repository = icr;
        }
        // GET: Cockpit/Customer
        public ActionResult Index()
        {
            var customers = _repository.GetAllCustomers();
            return View(customers);
        }

        //Creation d'un client 
        [HttpGet]
        [Route("create")]
        public ActionResult Create()
        {



            //  on recupère les villes ,les pays ,code postaux 
           // ViewData["Categories"] = _catrepository.GetAll();


            return View(new Customer());
        }

        [HttpPost]
        [Route("create")]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Customer model)
        {

            _repository.Create(model);
            return RedirectToAction("index");


        }

        //edition d'un client
        [HttpGet]
        [Route("edit/{customer_id}")]
        public ActionResult Edit(int customer_id)
        {
            //on recupère le client correspondant à l'id
            var customer = _repository.GetCustomerById(customer_id);
            
            //si le client existe pas dans la base 
            if (customer == null)
            {
                return HttpNotFound();
            }

            return View(customer);
        }

        [HttpPost]
        [Route("edit/{customer_id}")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Customer model, int customer_id)
        {
            //on recupère le client correspondant à l'id
            var customer = _repository.GetCustomerById(customer_id);
            //on verifie si le client existe 
            if (customer == null)
            {
                return HttpNotFound();
            }
            //on test l'etat du model 
            if (!ModelState.IsValid)
            {
                //on retourne la vue en lui passant le model
                return View(model);

            }

            //on fait la mise à jour du client
            _repository.Edit(model, customer_id);
            return RedirectToAction("index");

        }

        //suppression d'une categorie
        [Route("Remove/{customer_id}")]
        public ActionResult Remove(int customer_id)
        {

            var customer = _repository.GetCustomerById(customer_id);
            _repository.RemoveCustomer(customer_id);

            return RedirectToAction("index");
        }
    }
}