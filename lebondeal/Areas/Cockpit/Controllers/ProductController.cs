﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using lebondeal.Data;
using lebondeal.Models;
using lebondeal.Services;

namespace lebondeal.Areas.Cockpit.Controllers
{
    [RouteArea("Cockpit")]
    [RoutePrefix("Product")]
    [Authorize]
    public class ProductController : Controller
    {
        private readonly  IProductRepository _repository ;
        private readonly ICategoryRepository _catrepository;
        //les constructeurs
        public ProductController():this(new ProductRepository(), new CategoryRepository())
        { }
       
        public ProductController(IProductRepository ipr,ICategoryRepository ctr)
        {
            _repository = ipr;
            _catrepository = ctr;
        }
        // GET: Cockpit/Product
        [Route("")]
        public ActionResult Index()
        {
            //on recupère tous les produits
            var products = _repository.GetAll();
            return View(products);
        }
        //edition d'un produit
        [HttpGet]
        [Route("edit/{id}")]
        public ActionResult Edit(int productId)
        {
            //on recupère le produit correspondant à l'id
            var product = _repository.Get(productId);
            ViewData["Categories"] = _catrepository.GetAll();
            //si le produit existe pas dans la base 
            if (product == null)
            {
                return HttpNotFound();
            }

            return View(product);
        }

        [HttpPost]
        [Route("edit/{id}")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Product model,int productId, HttpPostedFileBase file)
        {
            //on recupère le produit correspondant à l'id
            var product = _repository.Get(productId);
            //on verifie si le produit existe 
            if (product == null)
            {
                return HttpNotFound();
            }
            //on test l'etat du model 
            if (!ModelState.IsValid)
            {
                //on retourne la vue en lui passant le model
                return View(model);

            }

            /*  if (String.IsNullOrWhiteSpace(postId))
              {

                  ModelState.AddModelError("key","message à afficher");
                    return View(model);
              }*/
            //on fait la mise à jour du produit
           

            try
            {
                if (file == null)
                {



                    model.ImgUrl = product.ImgUrl;
                }
                else
                {
                    string fullPath = Request.MapPath("~/Areas/Cockpit/Images/Produits" + product.ImgUrl);
                    if (System.IO.File.Exists(fullPath))
                    {
                        System.IO.File.Delete(fullPath);
                    }
                    string pic = System.IO.Path.GetFileName(file.FileName);

                    string path = System.IO.Path.Combine(
                                           Server.MapPath("~/Areas/Cockpit/Images/Produits"), pic);
                    // file is uploaded
                    file.SaveAs(path);
                    model.ImgUrl = file.FileName;
                }
                _repository.Edit(model, productId);
                return RedirectToAction("index");
            }
            catch (KeyNotFoundException e)
            {


                return HttpNotFound();
            }
            catch (Exception e)
            {

                ModelState.AddModelError("key", e);
                return View(model);
            }

        }

        //Creation d'un produit
        [HttpGet]
        [Route("create")]
        public  ActionResult Create()
        {

         

            //on recupère les categories 
            ViewData["Categories"] = _catrepository.GetAll();
         

            return View(new Product());
        }

        //suppression d'un produit
        [Route("Remove/{productId}")]
        public ActionResult Remove(int productId)
        {

            var product = _repository.Get(productId);
            _repository.RemoveProduct(productId);
            string fullPath = Request.MapPath("~/Areas/Cockpit/Images/Produits" + product.ImgUrl);
            if (System.IO.File.Exists(fullPath))
            {
                System.IO.File.Delete(fullPath);
            }

            return RedirectToAction("index");
        }

        [HttpPost]
        [Route("create")]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Product model, HttpPostedFileBase file)
        {
            
            //on test l'etat du model 
         /*   if (!ModelState.IsValid)
            {
                //on retourne la vue en lui passant le model
                return View(model);

            }*/

            try
            {
                if (file != null)
                {
                    string pic = System.IO.Path.GetFileName(file.FileName);

                    string path = System.IO.Path.Combine(
                                           Server.MapPath("~/Areas/Cockpit/Images/Produits"), pic);
                    // file is uploaded
                    file.SaveAs(path);
                    model.ImgUrl = file.FileName;

                    // save the image path path to the database or you can send image
                    // directly to database
                    // in-case if you want to store byte[] ie. for DB
                    /*   using (MemoryStream ms = new MemoryStream())
                       {
                           file.InputStream.CopyTo(ms);
                           byte[] array = ms.GetBuffer();
                       }*/

                }

                _repository.Create(model);
                return RedirectToAction("index");
            }
            catch (Exception e)
            {
                
                ModelState.AddModelError("key",e);
                return View(model);
            }
            
        }
    }
}