﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using lebondeal.Data;
using lebondeal.Models;

namespace lebondeal.Areas.Cockpit.Controllers
{
    [RouteArea("Cockpit")]
    [RoutePrefix("Category")]
    [Authorize]
    public class CategoryController : Controller
    {
        private readonly ICategoryRepository _repository;
        //les constructeurs
        public CategoryController():this(new CategoryRepository())
        { }

        public CategoryController(ICategoryRepository ipr)
        {
            _repository = ipr;
        }
        // GET: Cockpit/Category
        public ActionResult Index()
        {
            //on recupère tous les produits 
            var categories = _repository.GetAll();
            return View(categories);
           
        }

        //Creation d'un produit
        [HttpGet]
        [Route("create")]
        public ActionResult Create()
        {


            return View(new Category());
        }

        [HttpPost]
        [Route("create")]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Category model, HttpPostedFileBase file)
        {

            //on test l'etat du model 
               if (!ModelState.IsValid)
               {
                   //on retourne la vue en lui passant le model
                   return View(model);

               }

            try
            {
                if (file != null )
                {
                    string pic = System.IO.Path.GetFileName(file.FileName);

                    string path = System.IO.Path.Combine(
                                           Server.MapPath("~/Areas/Cockpit/Images/Categories"), pic);
                    // file is uploaded
                    file.SaveAs(path);
                    //on affecte le l'extension de l'image dans le model
                    model.CategoryImage = file.FileName;
                    // save the image path path to the database or you can send image
                    // directly to database
                    // in-case if you want to store byte[] ie. for DB
                    /*   using (MemoryStream ms = new MemoryStream())
                       {
                           file.InputStream.CopyTo(ms);
                           byte[] array = ms.GetBuffer();
                       }*/

                }

                _repository.Create(model);
                return RedirectToAction("index");
            }
            catch (Exception e)
            {

                ModelState.AddModelError("key", e);
                return View(model);
            }

        }

        //edition d'une catégorie
        [HttpGet]
        [Route("edit/{id}")]
        public ActionResult Edit(int categoryId)
        {
            //on recupère le produit correspondant à l'id
            var category = _repository.Get(categoryId);
            
            //si le produit existe pas dans la base 
            if (category == null)
            {
                return HttpNotFound();
            }

            return View(category);
        }

        [HttpPost]
        [Route("edit/{id}")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Category model, int categoryId, HttpPostedFileBase file)
        {
            //on recupère le produit correspondant à l'id
            var category = _repository.Get(categoryId);
            //on verifie si le produit existe 
            if (category == null)
            {
                return HttpNotFound();
            }
            //on test l'etat du model 
            if (!ModelState.IsValid)
            {
                //on retourne la vue en lui passant le model
                return View(model);

            }

            /*  if (String.IsNullOrWhiteSpace(postId))
              {

                  ModelState.AddModelError("key","message à afficher");
                    return View(model);
              }*/
            //on fait la mise à jour du produit


            try
            {
                if (file == null)
                {


                    model.CategoryImage = category.CategoryImage;

                }
                else
                {
                    string fullPath = Request.MapPath("~/Areas/Cockpit/Images/Categories" + category.CategoryImage);
                    if (System.IO.File.Exists(fullPath))
                    {
                        System.IO.File.Delete(fullPath);
                    }
                    string pic = System.IO.Path.GetFileName(file.FileName);

                    string path = System.IO.Path.Combine(
                        Server.MapPath("~/Areas/Cockpit/Images/Categories"), pic);
                    // file is uploaded
                    file.SaveAs(path);
                    model.CategoryImage = file.FileName;


                }
                _repository.Edit(model, categoryId);
                return RedirectToAction("index");
            }
            catch (KeyNotFoundException e)
            {


                return HttpNotFound();
            }
            catch (Exception e)
            {

                ModelState.AddModelError("key", e);
                return View(model);
            }

        }


        //suppression d'une categorie
        [Route("Remove/{categoryId}")]
        public ActionResult Remove(int categoryId)
        {

            var category = _repository.Get(categoryId);
            _repository.Remove(categoryId);
            string fullPath = Request.MapPath("~/Areas/Cockpit/Images/Categories" + category.CategoryImage);
            if (System.IO.File.Exists(fullPath))
            {
                System.IO.File.Delete(fullPath);
            }

            return RedirectToAction("index");
        }
    }
}